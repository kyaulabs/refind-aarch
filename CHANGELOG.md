### 1.000 (2019-07-29)

* Merged refind-numix-circle with background found on Reddit and agave font.

* README, CHANGELOG & LICENSE have been added.
