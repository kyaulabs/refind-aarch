![ANSI Logo](https://gitlab.com/kyaulabs/refind-aarch/raw/master/refind-aarch.ans.png "ANSI Logo")  
<a href="https://kyaulabs.com/">https://kyaulabs.com/</a>

[![](https://img.shields.io/badge/coded_in-vim-green.svg?logo=vim&logoColor=brightgreen&colorB=brightgreen&longCache=true&style=flat)](https://vim.org) &nbsp; [![](https://img.shields.io/badge/license-AGPL_v3-blue.svg?style=flat)](https://gitlab.com/kyaulabs/refind-aarch/blob/master/LICENSE)  
[![](https://img.shields.io/badge/pkg:refind--efi->=_0.11.2-8E68AC.svg?style=flat)](https://www.archlinux.org/packages/extra/x86_64/refind-efi/)

### About
A rEFInd theme that is heavily based upon refind-numix-circle using the icons
and palette from Numix Circle and the font `agave`. This is meant to be paired
with aarch and is automatically downloaded by said script.

### Usage
Find the `refind` directory on your ESP partition. This is the location that
refind was installed to and is usually located at `/boot/EFI/refind`

```shell
cd /boot/EFI/refind
```

Create a directory for themes if one does not already exist and then change to
it.

```shell
mkdir themes && cd themes
```

Clone this repository into said directory.
```shell
git clone https://gitlab.com/kyaulabs/refind-aarch
```

Modify your current `refind.conf` menuentries to reflect changes (icon
locations), then add the theme config to the end of the file.

```shell
echo "include themes/refind-aarch/theme.conf" >> /boot/EFI/refind/refind.conf
```

### Attribution
The background is [Colour Key Landscape Concept][link-background] by Jamil Dar.  
The font is [agave][link-font] by agaric.  
The icons and palatte are from [refind-numix-circle][link-numix].

[link-numix]: https://github.com/abazad/refind-numix-circle
[link-background]: https://www.artstation.com/artwork/lVNyKk
[link-font]: https://github.com/agarick/agave
